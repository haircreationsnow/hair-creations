Hair Creations was established almost 20 years ago by Irene in her boutique home salon and has grown to what you see and experience in her salon today. She has built a salon that is backed with a team of high values, continuous education alongside the best and healthiest products in the industry.

Address: 5205 King St, Beamsville, ON L0R 1B3, Canada

Phone: 905-708-4247

Website: http://www.haircreationsnow.com
